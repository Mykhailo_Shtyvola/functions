const getSum = (str1, str2) => {
  if(typeof str1 == "object" || typeof str2 == "object"){
    return false;
  }
  if(str1.replace(/[0-9]/g, '').length !== 0 || str2.replace(/[0-9]/g, '').length !== 0){
    return false;
  }
  if (str1.length > str2.length)
    {
        let t = str1;
        str1 = str2;
        str2 = t;
    }
     
    let str = "";
     
    let n1 = str1.length, n2 = str2.length;
     
    let strn1 = str1.split("").reverse().join("");
    let strn2 = str2.split("").reverse().join("");
     
    let carry = 0;
    for(let i = 0; i < n1; i++)
    {
         
        let sum = ((strn1[i].charCodeAt(0) -
                        '0'.charCodeAt(0)) +
                   (strn2[i].charCodeAt(0) -
                        '0'.charCodeAt(0)) + carry);
        str += String.fromCharCode(sum % 10 +
                        '0'.charCodeAt(0));
     
        carry = Math.floor(sum / 10);
    }
     
    for(let i = n1; i < n2; i++)
    {
        let sum = ((strn2[i].charCodeAt(0) -
                        '0'.charCodeAt(0)) + carry);
        str += String.fromCharCode(sum % 10 +
                        '0'.charCodeAt(0));
        carry = Math.floor(sum / 10);
    }
     
    if (carry > 0)
        str += String.fromCharCode(carry +
                       '0'.charCodeAt(0));
     
    str = str.split("").reverse().join("");
     
    return str;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for(let i = 0; i < listOfPosts.length; i++){
    if(listOfPosts[i].comments){
      for(let j = 0; j < listOfPosts[i].comments.length; j++){
        if(listOfPosts[i].comments[j].author == authorName){
          comments++;
        }
      }
    }
    if(listOfPosts[i].author == authorName){
      posts++;
    }
  }
  return "Post:"+posts+",comments:"+comments;
};

const tickets=(people)=> {
  const price = 25;
  let current = [];
  for(let i = 0; i < people.length; i++){
    if(people[i] == 25){
      current.push(people[i]);
    }
    if(people[i] == 50){
      if(current.lastIndexOf(25) != -1){
        current.splice(current.lastIndexOf(25));
        current.push(people[i]);
      }
      else{
        return 'NO';
      }
    }
    if(people[i] == 100){
      if(current.filter(x => x == 25).length > 2){
        current.splice(current.lastIndexOf(25));
        current.splice(current.lastIndexOf(25));
        current.splice(current.lastIndexOf(25));
      }
      else if(current.lastIndexOf(25) != -1 && current.lastIndexOf(50) != -1){
        current.splice(current.lastIndexOf(25));
        current.splice(current.lastIndexOf(50));
      }
      else{
        return 'NO';
      }
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
